
#include <stdio.h>
#include <stdlib.h>

// include standard library macro for bool
#include <stdbool.h>

int doubleNat(int(*f)(int), int x){
	return f(f(x));
}

bool doubleRcd(bool(*f)(bool), bool x){
	return f(f(x));
}

// example function for call with doubleNat
int incr(int x) {
	return ++x;
}

// example function for call with doubleRcd
bool neg(bool x) {
	return !x;
}

int main(void) {

	int i = doubleNat(incr,5);
	bool b = doubleRcd(neg,true);

	printf("int i: %d; bool b: %d",i,b);
	// int i: 7; bool b: 1

	return 0;

}
